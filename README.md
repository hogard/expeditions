# Expeditions #

Source of a two-player card-game web-application project which is available at http://hogard.codes/expeditions/ as of June 1st 2016.

### Description ###

* Two-player single-page card-game written with a simple custom ui.
* Single-page web-application that tracks game-play without refreshing, utilizing jQuery and AJAX.
    * OOP approach in which players interact with the game and it's children (table, deck, discard piles).
* Tracks user turn, score, and hand.
    * Cross-browser play is not yet supported, turn tracking is primarily for testing purposes.

### Currently Under Development ###

* cross_browser Branch
    * Creation of a registration/ account creation system
    * Initial preparation for cross-browser game-play.

### Set up and Testing ###

* Code is predominantly written in PHP and Javascript.
* Running on PHP version 7.0.4

### Contact ###

* Suggestions for improvement and additional questions/ concerns may be directed to ryanhogard@gmail.com