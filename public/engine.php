<?php
	require_once('../includes/autoload.inc.php');
	require_once('graphics.php');
	session_start();

	$game   = $_SESSION['game'];
	$player = $game->getTurn();

	if ( $player == "playerA" ) {
		$rival = "playerB";
	} elseif ( $player == "playerB" ) {
		$rival = "playerA";
	}

	if ( isset($_GET['pend']) && $game->stage == 1 ) {
		$pend = json_decode(($_GET['pend']), true);
		$game->$player->pendCard($pend);
	}

	if ( isset($game->$player->pend) ) {
		$pend = $game->$player->pend;
	}

	if ( isset($_GET['pendToss']) ) {
			$flop = json_decode($_GET['pendToss'], true);
			$game->flopCard($player, "disCard", $flop);
			$game->progress();
			$game->$player->clearPend();
			unset($pend);
		} elseif ( isset($_GET['pendPlay']) ) {
			$flop = json_decode($_GET['pendPlay'], true);
			$game->flopCard($player, "useCard", $flop);
			$game->progress();
			$game->$player->clearPend();
			unset($pend);
		}	

	if ( isset($_GET['draw']) && $game->stage == 2 ) {
		$draw = json_decode($_GET['draw'], true);
		$type = $draw['type'];
		$game->progress();
		$game->drawCard($player, $type);
		$game->endTurn();
	}
	
?>