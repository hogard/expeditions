<?php require_once('engine.php'); ?>

<div id='rival'>
  <div class='column'></div>
  <div id='playerH' class='column'>
    <?php buildRun('H', $game, $rival); ?>
  </div>
  <div id='playerE' class='column'>
    <?php buildRun('E', $game, $rival); ?>
  </div>
  <div id='playerB' class='column'>
    <?php buildRun('B', $game, $rival); ?>
  </div>
  <div id='playerA' class='column'>
    <?php buildRun('A', $game, $rival); ?>
  </div>
  <div id='playerZ' class='column'>
    <?php buildRun('Z', $game, $rival); ?>
  </div>
</div>

<div id='discard'>
  	<div id='deck' class='column'>
  	  <?php buildDeck($game); ?>
  	</div>
	<div id='H' class='column'>
	  <?php buildDiscard('H', $game); ?>
    </div>
	<div id='E' class='column'>
	  <?php buildDiscard('E', $game); ?>
	</div>
	<div id='B' class='column'>
	  <?php buildDiscard('B', $game); ?>	
	</div>
	<div id='A' class='column'>
	  <?php buildDiscard('A', $game); ?>
	</div>
	<div id='Z' class='column'>
	  <?php buildDiscard('Z', $game); ?>
	</div>
</div>

<div id='player'>
  <div class='column'></div>
  <div id='playerH' class='column'>
  	<?php buildRun('H', $game, $player); ?>
  </div>
  <div id='playerE' class='column'>
  	<?php buildRun('E', $game, $player); ?>
  </div>
  <div id='playerB' class='column'>
  	<?php buildRun('B', $game, $player); ?>
  </div>
  <div id='playerA' class='column'>
  	<?php buildRun('A', $game, $player); ?>
  </div>
  <div id='playerZ' class='column'>
  	<?php buildRun('Z', $game, $player); ?>
  </div>
</div>

<div id="hand">
  <div id='stats'>
  <span id="terminal">
  <?= $player; ?>
  </span>
      <pre id="rules" class="action">
	      
	      	    
.------------.
: Rules :
'------------'</pre>
  </div>

  <div id="fan">
    <?php
      foreach ($game->$player->hand as $card) {
	    buildCard($card, '');
      } 
    ?>
  </div>

  <div id="onDeck">
    <?php
      if ( isset($pend) && $pend != '' ) {
      	buildCard($pend, 'pend');
      }
    ?>
  </div>

  <div id="acts">
  <?php if( $player == $game->getTurn()) { ?>
    <pre id="preplay" class="action">
.--------------.
: Play :
'--------------'</pre><pre id="pretoss" class="action">
.--------------.
: Toss :
'--------------'</pre>
  <?php } else { ?>
    <pre id="endplay" class="action">

.--------------.
: Done :
'--------------'</pre>
  <?php } ?>
  </div>
</div>
<?php
$_SESSION['game'] = $game;
outputFieldTab('fieldA', $game);
?>