<?php require_once('engine.php'); ?>

<span id= "terminal">
<div id ="instructions">
<p>Expeditions: A two player card game with a slightly-less-than exhilarating point-and-click interface!</p></br>

<p>Game-play:</br>
 <div>
    - At the start of the game each player is dealt a hand of eight cards.</br>
    - There are five "suits" H, B, E, A, and Z. Each suit contains cards valued two through ten
    and three "wager" cards for a total of sixty cards.</br>
    - On a players turn, they must select (click) a card from their hand and select (click) "play" or "toss".</br>
    - - If the player selects "play" the card leaves their hand, and appears face up on their side of the board.</br>
    - - - Cards must be played in ascending order, with wager cards representing the lowest, and
    ten representing the highest value of the suit.</br>
    - - If the player selects "toss" the card leaves their hand, and appears face up in one of five tossed piles.</br>
    - After choosing an action, a player may either draw (click) a card from the remaining deck, or pull (click)
    a card from a discard pile.</br>
    - After the player once again has eight cards in their hand, they must select (click) "done".</br>
    - Clicking done hides the players hand, enabling them to safely transfer the screen to their opponent's view.*</br>
    - The opponent, must select "transfer", rotating the board and revealing their hand.</br></br>
    - The above process is repeated until there are no remaining cards in the deck.</br></br>
  </div>
</p>

<p>Scoring:</br>
 <div>
    - Playing a card in a category ( H, B, E, A, or Z) "costs" 20 points. Each card played in a
    category "earns" the value of the played card.</br>
    - - If "2" is the only card played in a player's "H" category the total score for this category is -18.</br>
    - If no cards are played in a category, this category does not incur a 20 point cost.</br>
    - Playing a "wager" card in a category doubles the earned points in this category AFTER it has been subtracted from
    the initial 20 point cost.</br>
    - - If "Wager, 5, 8, 9, and 10" are played in a player's "A" category, the total score for this category is +26.</br>
    - - Inversely, if "Wager, 6, 7" are played in a player's "Z" category, the total score for this category is -14.</br></br>
    - The game is automatically scored when a player selects (clicks) "transfer" and the deck is empty.</br></br>
  </div>

*The game requires two players whom, in the current version, share a view of the game table. After clicking
continue, the players should orient themselves so that only one is able to see the screen at a time. Until
cross-browser play is supported, the best way to accomplish this is to rotate the screen back and forth
between turns (sorry, I'm working on it).</br>
**Expeditions is a two-player variant of solitaire which has been recently popularized by Renier Knizia.</br>
</div>
</span>

  <span id="start" class="center">
  <pre>.--------------.
:    Return    :
'--------------'
  </pre>
  </span>