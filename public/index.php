<!DOCTYPE html>
<html>

<head>
<title>Expeditions</title>
  <link rel="stylesheet" type="text/css" href="css/main.css"> 
  <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>
</head>

<body>

<div id ="interface" class="center">
  <div id="header"></div>
  <div id="content"></div>
</div>

</body>

</html>