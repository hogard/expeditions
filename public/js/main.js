jQuery(document).ready(function(){
	$('#content').load('menu.php');
	$('#header').load('header.php');
	

	$('#interface').delegate('#start' , 'click', function(){
		$('#content').load('table.php');
	});
	
	$('#interface').delegate('#rules' , 'click', function(){
		$('#content').load('howto.php');
	});

	$('#interface').delegate('#hand .card', 'click', function(){
		var play = $(this).attr('value');
			$.ajax({
                    type: "GET", url: 'table.php', data: "pend="+play,
                    success: function(data) { $('#content').html(data); }
                });
	});

	$('#interface').delegate('#preplay', 'click', function(){
		var play = $('#pend').attr('value');
		if (typeof play !== 'undefined' && play !== null) {
   			$.ajax({
                    type: "GET", url: 'table.php', data: "pendPlay="+play,
                    success: function(data) { $('#content').html(data); }
            });
        }
	});

	$('#interface').delegate('#pretoss', 'click', function(){
		var play = $('#pend').attr('value');
		if (typeof play !== 'undefined' && play !== null) {
   			$.ajax({
                type: "GET", url: 'table.php', data: "pendToss="+play,
                success: function(data) { $('#content').html(data); }
            });
        }
	});

	$('#interface').delegate("#discard .column .card", 'click', function(){
		var draw = $(this).attr('value');
			$.ajax({
                type: "GET", url: 'table.php', data: "draw="+draw,
                success: function(data) { $('#content').html(data); }
            });
	});

	$('#interface').delegate('#endplay', 'click', function(){
		var draw = $('#discard .column .card').attr('id');
		if ( draw != 0 ) {
			$('#hand').load('transfer.php');
		} else {
			$('#hand').load('tabulate.php');
		}
	});

	$('#interface').delegate('#transfer', 'click', function(){
		$('#content').load('table.php');
			$.ajax({
	                type: "GET", url: 'table.php', data: "check=true",
	                success: function(data) { $('#content').html(data); }
	            });
	});
});