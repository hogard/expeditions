
<?php require_once('engine.php'); ?>
<!--Tabulate and display scores, identify winner-->
<h3>Expedition Over</h3>
<div id="playerA" class="tabulate">
Player A <?= outputFieldTab('fieldA', $game) ?>
  <?php
    echo outputColumnTab('fieldA', 'Hyperborea', $game);
    echo outputColumnTab('fieldA', 'Buyan', $game);
    echo outputColumnTab('fieldA', 'Eden', $game);
    echo outputColumnTab('fieldA', 'Agartha', $game);
    echo outputColumnTab('fieldA', 'Zerzura', $game);
  ?> 
</div>
<div id="victory" class="tabulate">
<?= outputVictory($game); ?>
</div>
<div id="playerB" class="tabulate">
Player B <?= outputFieldTab('fieldB', $game); ?>
  <?php
    echo outputColumnTab('fieldB', 'Hyperborea', $game);
    echo outputColumnTab('fieldB', 'Buyan', $game);
    echo outputColumnTab('fieldB', 'Eden', $game);
    echo outputColumnTab('fieldB', 'Agartha', $game);
    echo outputColumnTab('fieldB', 'Zerzura', $game);
   ?> 
</div>