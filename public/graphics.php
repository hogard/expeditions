<?php
//Messy file, but still better than creating custom graphics.


function buildCard($card, $role) {

	$cardJson  = json_encode($card);

	if ( $role == 'field' ) {
		$topLine = '';
	} else {
		$topLine = "<div class='card' id='$role' value='$cardJson'><pre> _______ ";
	}

	if ( $card['value'] == 1 ) {
		$line = "Wager";
	} elseif ( $card['value'] == 10 ) {
		$line = "  " . $card['value'] . " ";
	} else {
		$line = "  " . $card['value'] . "  ";
	}

	echo 
"$topLine
| $line |
|       |
|       |
|       |
|   " . $card['type'] . "   |
'-------'</pre></div>";
}

function buildColumn($column, $role) {

	if ( count($column) > 0 ) {
		$top = array_pop($column);
		$cardJson  = json_encode($top);

		if ( count($column) > 0 ) {
			$lines = '';
			foreach ($column as $card) {
				if ( $card['value'] == 1 ) {
					$lines = $lines . "</br>|_Wager_|";
				} elseif ( $card['value'] == 10 ) {
					$lines = $lines . "</br>|___" . $card['value'] . "__|";
				} else {
					$lines = $lines . "</br>|___" . $card['value'] . "___|";
				}
			}
		} else {
			$lines = '';
		}
		echo "<div class='card' id='$role' value='$cardJson'><pre> _______ $lines";
		buildCard($top, $role);
	}
}

function buildDiscard($column, $game) {
	if ( !empty($game->table->fieldD->columns[$column]) ) {
      	  $top = count($game->table->fieldD->columns[$column]) - 1;
	      buildCard($game->table->fieldD->columns[$column][$top], '');
		}
}

function buildRun($column, $game, $user) {
	if ( $user == "playerA" ) {
		$field = "fieldA";
	} else {
		$field = "fieldB";
	}

	if ( !empty($game->table->$field->columns[$column]) ) {
	      buildColumn($game->table->$field->columns[$column], 'field');
		}
}

function buildDeck($game) {
	$count = $game->table->deck->countCards();
	$cardJson = json_encode(array ( 'type' => 'D'));

	echo "<div id='$count' class='card' value='$cardJson'><pre>
   _______
  | Deck  |
  |       |
     $count
  |       |
  |       |
  '======='</pre></div>";

}

function tabulateColumn($field, $type, $game) {
	$total = 0;
	$wager = 1;
	$run = $game->table->$field->columns[$type];
	if ( count($run) == 0 ) {
		return 0;
	} else {
		foreach ( $run as $card ) {
			if ( $card['value'] == 1 ) {
				$wager++;
			} else {
				$total += $card['value'];
			}
		}
	}
	return ( ( $total - 20 ) * $wager );
}

function outputColumnTab($field, $column, $game) {
	$type = $column[0];
	$final = tabulateColumn($field, $type, $game);

	if ( $final == 0 ) {
		return '';
	} else {
		return $column . " Expedition: " . $final . " points. </br>";
	}
	
}

function tabulateField($field, $game) {
	$total = 0;
	$columns = $game->table->$field->columns;

	foreach ( $columns as $type => $column ) {
		$add = tabulateColumn($field, $type, $game);
		$total += $add;
	}
	return $total;
}

function outputFieldTab($field, $game) {
	$total = tabulateField($field, $game);

	return "Cummaltive: " . $total . " points. </br>";
}

function viewHand($hand) {
	foreach ( $hand as $card ) {
		echo $card['type'] . ": " . $card['value'] . " | ";
	} 
	echo "</br>"; 
}

function outputVictory($game) {
	if ( tabulateField( 'fieldA', $game ) > tabulateField( 'fieldB', $game ) ) {
		return 'Player A emerges as the superior navigator.';
	} elseif ( tabulateField( 'fieldB', $game ) > tabulateField( 'fieldA', $game ) ) {
		return 'Player B emerges as the superior navigator.';
	} else {
		return 'This outcome is incredibly unlikely.';
	}
}

function viewTable($table) {
	echo "</br>D: ";
	print_r($table->fieldD->columns);
	echo "</br>A: ";
	print_r($table->fieldA->columns);
	echo "</br>B: ";
	print_r($table->fieldB->columns);
	echo "</br></br>Cards Remaining: " . $table->deck->countCards();
}
?>