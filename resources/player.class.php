<?php

class player
{
	public $hand = array();
	public $pend;

	public function __construct($deck)
	{
		while ( count($this->hand) < 8 ) {
			array_push($this->hand, $deck->drawCard());
		} 
	}

	public function pullCard($card)
	{
		if ( $card !== false ) {
			$index = array_search( $card, $this->hand );
				unset($this->hand["$index"]);
				$this->hand = array_values($this->hand);
		}
	}

	public function pushCard($card)
	{
		array_push( $this->hand, $card);
	}

	public function pendCard($card)
	{
		if ( isset($this->pend) ) {
			self::pushCard($this->pend);
		}
		self::pullCard($card);
		$this->pend = $card;
	}

	public function clearPend()
	{
		unset($this->pend);
	}
}

?>