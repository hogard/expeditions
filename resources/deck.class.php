<?php

class deck
{
	public $stack;

	public function __construct()
	{
		$count  = 1;
		$types  = array ( "H", "B", "E", "A", "Z" );
		$values = array ( 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

		foreach ($types as $type) {
			foreach ($values as $value) {
				$this->stack[] = array (
					"id" => $count++, "value" => $value, "type" => $type
					);
			}
		} 

		shuffle($this->stack);
	}

	public function countCards()
	{
		return count($this->stack);
	}

	public function drawCard()
	{
		return array_shift($this->stack);
	}
}

?>