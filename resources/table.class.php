<?php

class table 
{
	public $deck;

	public $fieldD;
	public $fieldA;
	public $fieldB;

	public function __construct()
	{
		$this->deck   = new deck;
		
		$this->fieldD = new field;
		$this->fieldA = new field;
		$this->fieldB = new field;
	}

	public function disCard($player, $card)
	{
		$this->fieldD->tossCard($card);
		return $card;
	}

	public function useCard($player, $card)
	{
		if ( $player == "playerA" ){
			return $this->fieldA->playCard($card);
		} else {
			return $this->fieldB->playCard($card);
		}
	}

	public function drawCard($type)
	{
		if ( $type == "D" ) {
			return $this->deck->drawCard();
		} else {
			return $this->fieldD->drawCard($type);
		}
	}
}

?>