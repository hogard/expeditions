<?php

class game
{
	public $table;
	public $playerA;
	public $playerB;

	public $stage;
	public $round;

	public function __construct()
	{
		$this->table   = new table;
		$this->playerA = new player($this->table->deck);
		$this->playerB = new player($this->table->deck);
		$this->stage   = 1;
		$this->round   = 1;
	}

	public function flopCard($player, $action, $card)
	{
		$card = $this->table->$action( $player, $card );
		$this->$player->pullCard($card);
	}

	public function drawCard($player, $type)
	{
		$card = $this->table->drawCard( $type );
		$this->$player->pushCard($card);
	}

	public function progress()
	{
		$this->stage++;
	}

	public function endTurn()
	{
		$this->round++;
		$this->stage = 1;
		unset($this->playerA->pend);
		unset($this->playerB->pend);
	}

	public function getTurn()
	{
		if ( $this->round % 2 == 0 ) {
			return "playerB";
		} else {
			return "playerA";
		}
	}
}

?>