<?php

class field
{
	public $columns;

	public function __construct()
	{
		$this->columns = array( "H" => array(), 
								"B" => array(), 
								"E" => array(), 
								"A" => array(), 
								"Z" => array() );
	}

	public function drawCard($type)
	{
		return array_pop($this->columns["$type"]);
	}

	public function tossCard($card)
	{
		$type = $card['type'];
		array_push($this->columns["$type"], $card);
	}

	public function playCard($card)
	{
		$type = $card['type'];

		foreach ( $this->columns["$type"] as $play ) {
			if ( $card['value'] < $play['value']) {
				return false;
			}
		}

		array_push($this->columns["$type"], $card);
		return $card;
	}
}

?>