<?php
spl_autoload_register('autoloader::resourcesloader');

class autoloader
{
	public static function resourcesLoader($class_name)
	{
		$path = __DIR__ . '/../resources/';

		include $path.$class_name.'.class.php';
	}
}

?>